package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func main() {
	// /にアクセスされたらhello()関数が動く
	http.HandleFunc("/", hello)
	// ポート番号8080で起動
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func hello(w http.ResponseWriter, r *http.Request) {
	// トークンを取得
	tokenBytes, err := ioutil.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/token")
	// byte配列を文字列に変換
	token := string(tokenBytes)
	// トークンを取得できなかった場合は空文字で代用
	if err != nil {
		token = ""
	}
	// HTTPリクエストをタイムアウト時間付きで作成
	ctx, cancel := context.WithTimeout(context.Background(), 10 * time.Second)
	defer cancel()
	req, err := http.NewRequest("GET", "https://kubernetes/api/v1/namespaces/default/pods", nil)
	if err != nil {
		sendResponseAndLog(w, 500, fmt.Sprintf("Error while creating request: %s", err.Error()))
	}
	req = req.WithContext(ctx)
	// リクエストのAuthorizationヘッダーにトークンを指定
	authHeader := fmt.Sprintf("Bearer %s", token)
	log.Printf("Authorization header value: %s\n", authHeader)
	req.Header.Add("Authorization", authHeader)
	// X.509証明書を取得
	caCertPool := x509.NewCertPool()
	caCert, err := ioutil.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/ca.crt")
	if err != nil {
		sendResponseAndLog(w, 500, fmt.Sprintf("Error while reading CA cert: %s", err.Error()))
	}
	caCertPool.AppendCertsFromPEM(caCert)
	// kube-apiserverにアクセス
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: caCertPool,
			},
		},
	}
	resp, err := client.Do(req)
	// エラー時は500でレスポンス
	if err != nil {
		sendResponseAndLog(w, 500, fmt.Sprintf("Error while sending request: %s", err.Error()))
	}
	// レスポンスボディを取得
	body, err := ioutil.ReadAll(resp.Body)
	// エラー時は500でレスポンス
	if err != nil {
		sendResponseAndLog(w, 500, fmt.Sprintf("Error while reading response: %s", err.Error()))
	}
	defer resp.Body.Close()
	// 正常時は200で取得したレスポンスボディをそのままレスポンス
	sendResponseAndLog(w, 200, string(body))
}

func sendResponseAndLog(w http.ResponseWriter, status int, body string) {
	w.WriteHeader(status)
	fmt.Fprintf(w, body)
	log.Println(body)
}
