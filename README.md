K8s Security Sample App
=======================

Kubernetesのセキュリティ機能を体験するためのサンプルアプリです。

# エンドポイント一覧
- `/`
    - kube-apiserverにアクセスしてPodの一覧を取得して、それをそのまま返します。

# Dockerイメージ
registry.gitlab.com/casareal-learning-service/training/k8s-security-sample-app:latest
